<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class ExcelReader extends Controller
{
    public function numberExtraction($string){
        $output_string = "";
        $ext_output_string = "";
        for ($i = 0; $i < strlen($string); $i++) {
            if($i==0 || $i==1 || $i==2){
                    $output_string .= $string[$i];
            }else if($i==3){
                    $output_string .=")" . $string[$i];
            }else if($i==4 || $i==5){
                    $output_string .=$string[$i];
            }else if($i==6){
                    $output_string .= "-" .$string[$i];
            }else if($i==7 || $i==8 || $i==9){
                    $output_string .=$string[$i];
            }else {
                    $ext_output_string .=$string[$i];
            }
        }
        if($output_string){
            $output_string = "(".$output_string;
        }
        
        return array($output_string, $ext_output_string);
    }
    
    public function zipCode($string){
        $output = "";
        if(ctype_digit($string)){
            if(strlen($string) ==  5 || strlen($string)== 9){
                $output = $string;
            }
        }else if(ctype_alnum($string)){
            if(strlen($string) == 6){
                $output = $string;
            }
        }
        return $output;
    }
        
    public function readExcel(Request $request){
        $request->validate([
            'excel_file' => 'required|max:10240',
        ]);
        
        //Converted data
        $excel_data= array();
        $excel_data[]= array('Customer Certificate Holders');
        $excel_data[]= array('Group Name','Name 1','Name 2','Contact Name','Address 1','Address 2','City','State','Zip','Business Phone','Business Ext','Fax Phone','Fax Ext','Email','Description of Operations','Job Type','Job #','Project End Date','Licensed?','Bonded?','Export Reference #','# of Days','Method of Distribution','X over\'endeavor to\'' ,'X over\'But failure...through rep\'','X over\'Workers comp and...\'',
            'WOS for GL','WOS for Auto','WOS for Garage Liab','WOS for Garage Keepers Liab','WOS for Umbrella/Excess','WOS for Work Comp','WOS for Other','Addl Insd GL','Addl Insd Auto','Addl Insd Garage Liab','Addl Insd Garage Keepers','Addl Insd Umbrella/Excess','Addl Insd Other');
        
        //Exception data
        $exception_excel_data = array();
        $exception_excel_data[] = array('Customer Certificate Holders');
        $exception_excel_data[] = array('Group Name','Name 1','Name 2','Contact Name','Address 1','Address 2','City','State','Zip','Business Phone','Business Ext','Fax Phone','Fax Ext','Email','Description of Operations','Job Type','Job #','Project End Date','Licensed?','Bonded?','Export Reference #','# of Days','Method of Distribution','X over\'endeavor to\'' ,'X over\'But failure...through rep\'','X over\'Workers comp and...\'',
            'WOS for GL','WOS for Auto','WOS for Garage Liab','WOS for Garage Keepers Liab','WOS for Umbrella/Excess','WOS for Work Comp','WOS for Other','Addl Insd GL','Addl Insd Auto','Addl Insd Garage Liab','Addl Insd Garage Keepers','Addl Insd Umbrella/Excess','Addl Insd Other');
        //Discription of Opertaion
        $doo_exception_excel_data = array();
        $doo_exception_excel_data[] = array('Customer Certificate Holders');
        $doo_exception_excel_data[] = array('Group Name','Name 1','Name 2','Contact Name','Address 1','Address 2','City','State','Zip','Business Phone','Business Ext','Fax Phone','Fax Ext','Email','Description of Operations','Job Type','Job #','Project End Date','Licensed?','Bonded?','Export Reference #','# of Days','Method of Distribution','X over\'endeavor to\'' ,'X over\'But failure...through rep\'','X over\'Workers comp and...\'',
            'WOS for GL','WOS for Auto','WOS for Garage Liab','WOS for Garage Keepers Liab','WOS for Umbrella/Excess','WOS for Work Comp','WOS for Other','Addl Insd GL','Addl Insd Auto','Addl Insd Garage Liab','Addl Insd Garage Keepers','Addl Insd Umbrella/Excess','Addl Insd Other');
        
        libxml_use_internal_errors(true);
        
        $file = $request->file('excel_file');
        $group_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        
        $path = $file->getRealPath();
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($path);
        $reader->setReadDataOnly(TRUE);
        $spreadsheet = $reader->load($path);
        $workbook = $spreadsheet->getActiveSheet()->toArray();
        
        //create spreadsheet
        $expection_spreadsheet = new Spreadsheet();
        $doo_expection_spreadsheet = new Spreadsheet();

        $expection_row_count = 3;
        $doo_expection_row_count =3;
        //return $workbook;
       
        foreach($workbook as $key=>$column_value) {
            $exception_flag = false;
            $doo_exception_flag = false;
            
            if($key == 0 || $key == 1){
                continue;
            }
                //Zip code
                $zip_replace_value = preg_replace("/[^A-Za-z0-9]/", "",  $column_value['6']);
                $zip_rule = $this->zipCode($zip_replace_value);

                //Method of distrubution
                $column_value['11'] = preg_replace("/[^A-Za-z]/", "",  $column_value['11']);
                
                //Business Phone accept only digit
                $business_number_digits = preg_replace("/[^0-9]/", "",  $column_value['8']);
                $business_number_extraction = $this->numberExtraction($business_number_digits);
                
                //Fax Phone accept only digit
                $fax_number_digits = preg_replace("/[^0-9]/", "",  $column_value['9']);
                $fax_number_extraction = $this->numberExtraction($fax_number_digits);
                
                //Discription of operation
                $description_of_operation = trim($column_value['12'] ." ". $column_value['13'] ." ". $column_value['14'] ." " . $column_value['15'] ." ". $column_value['16']);
                $wordwrap = explode("\n", wordwrap($description_of_operation, 150));
                $description_of_operation_wordwrap = "";
                $description_of_operation_restwordwrap = "";
                foreach($wordwrap as $key=>$value){
                    if($key==0){
                        $description_of_operation_wordwrap = $value;
                    }else{
                        $description_of_operation_restwordwrap .=$value;
                    }
                }
                
                //Address 1 and Address 2
                $address_stirng = array("Avenue", "Boulevard", "Building", "Circle","Court","Expressway","Freeway","Lane","Parkway","Road","Square","Street","Suite","Turnpike","North","East","South","West","Northeast","Southeast","Southwest","Northwest","Highway");
                $address_abbrevation = array("AVE", "BLVD", "BLDG","CIR","CT","EXPY","FWY","LN","PKY","RD","SQ","ST","STE","TPKE","N","E","S","W","NE","SE","SW","NW","HWY");
                if(strlen(trim($column_value['2']))>30){
                    $column_value['2'] = str_ireplace($address_stirng, $address_abbrevation, $column_value['2']);
                }
                if(strlen(trim($column_value['3']))>30){
                    $column_value['3'] = str_ireplace($address_stirng, $address_abbrevation, $column_value['3']);
                }
                
              
                $data = array(
                    'Group Name' => $group_name,
                    'Company' => $column_value['1'],
                    'Name' => $column_value['0'],
                    'Contact Name' => '',
                    'Address1' => $column_value['2'],
                    'Address2' => $column_value['3'],
                    'City' => $column_value['4'],
                    'State' => $column_value['5'],
                    'Zip' => ((strlen($zip_rule) == 5 || strlen($zip_rule) ==9 ) ? preg_replace('/[^A-Za-z0-9\-\s]/', "",   $column_value['6']) : $column_value['6']),
                    'Phone' => $business_number_extraction[0],
                    'Business Ext' => $business_number_extraction[1],
                    'Fax' =>  $fax_number_extraction[0],
                    'Fax Ext' => $fax_number_extraction[1],
                    'Email' => $column_value['10'],
                    'Description of Operations' => $description_of_operation_wordwrap,
                    'Job Type' => '',
                    'Job #' => '',
                    'Project End Date' => '',
                    'Licensed?' => '',
                    'Bonded?' => '',
                    'Export Reference #' => '',
                    '# of Days' => '',
                    'Method of distrubution' => $column_value['11'],
                    'X over endeavor to' => '',
                    'X over But failure...through rep' => '',
                    'X over Workers comp and...' => '',
                    'WOS for GL' => ($column_value['22'] ? ((strtolower($column_value['22']) == "false") ? "N" : "Y") : "N"),
                    'WOS for Auto' => ($column_value['28'] ? ((strtolower($column_value['28']) == "false") ? "N" : "Y") : "N"),
                    'WOS for Garage Liab' => '',
                    'WOS for Garage Keepers Liab' => '',
                    'WOS for Umbrella/Excess' => ($column_value['34'] ? ((strtolower($column_value['34']) == "false") ? "N" : "Y") : "N"),
                    'WOS for Work Comp' => ($column_value['40'] ? ((strtolower($column_value['40']) == "false") ? "N" : "Y") : "N"),
                    'WOS for Other' => ($column_value['45'] ? ((strtolower($column_value['45']) == "false") ? "N" : "Y") : "N"),
                    'Addl Insd GL' => ($column_value['19'] ? ((strtolower($column_value['19']) == "false") ? "N" : "Y") : "N"),
                    'Addl Insd Auto' => ($column_value['25'] ? ((strtolower($column_value['25']) == "false") ? "N" : "Y") : "N"),
                    'Addl Insd Garage Liab' => '',
                    'Addl Insd Garage Keepers' => '',
                    'Addl Insd Umbrella/Excess' => ($column_value['31'] ? ((strtolower($column_value['31']) == "false") ? "N" : "Y") : "N"),
                    'Addl Insd Other' => ($column_value['43'] ? ((strtolower($column_value['43']) == "false") ? "N" : "Y") : "N"),
                );
                
               
            // exception rules                
             //group name   
            if(strlen(trim($group_name))>150){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('A'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            //company name(name1)
            if(strlen($column_value['1'])<1 || strlen(trim($column_value['1']))>51){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('B'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            //name(name2) 
            if((strlen(trim($column_value['0']))>51)){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('C'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            //address 1 
            if((strlen(trim($column_value['2']))>30)){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('E'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            //address 2
            if((strlen(trim($column_value['3']))>30)){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('F'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            //city
            if((strlen(trim($column_value['4']))>19)){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('G'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            //state
            if((strlen(trim($column_value['5']))>2)){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('H'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            
            //Zip
            if((!$zip_rule && strlen($column_value['6'])>0) || ($zip_rule && strlen($zip_rule) == 6 &&  preg_match("/[^A-Z0-9a-z\s]/", trim($column_value['6'])))){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('I'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
             
            //Business Phone
            if(strlen($business_number_digits)>0 && strlen($business_number_digits)<10){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('J'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFFF0000');
            }
            //Business Phone Ext
            if(strlen($business_number_extraction[1])>4){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('K'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFFF0000');
            }
            //Fax Phone
            if(strlen($fax_number_digits)>0 && strlen($fax_number_digits)<10){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('L'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFFF0000');
            }
            //Fax Phone Ext
            if(strlen($fax_number_extraction[1])>4){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('M'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFFF0000');
            }
            //Email
            if((strlen(trim($column_value['10']))>150)){
                $exception_flag = true;
                $expection_spreadsheet->getActiveSheet()->getStyle('N'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            //description of operation
            if(strlen($description_of_operation)>150){
                $doo_exception_flag = true;
                $doo_expection_spreadsheet->getActiveSheet()->getStyle('O'.$doo_expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            }
            //method of distribution
            if(strlen($column_value['11'])>0 && $column_value['11'] != 'Email' && $column_value['11'] != 'Fax' && $column_value['11'] != 'Print'){
                    $exception_flag = true;
                    $expection_spreadsheet->getActiveSheet()->getStyle('W'.$expection_row_count)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFFF0000');
            }
            
            if($exception_flag){
                $exception_excel_data[] = $data;
                $expection_row_count++;
            }else{
                $excel_data[] = $data;
            }
            
            $data['Description of Operations'] = $description_of_operation_restwordwrap;
            if($doo_exception_flag){
                $doo_exception_excel_data[] = $data;
                $doo_expection_row_count++;
            }
            
        }
        
            // Generated data
            $file_path = '';
            if(!empty($excel_data) && $excel_data>2){
                $converted_spreadsheet = new Spreadsheet(); // ADD DATA TO SPECIFIC CELL
                $converted_spreadsheet->getActiveSheet()->fromArray($excel_data, NULL, 'A1');
                $converted_writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($converted_spreadsheet);
                if (!file_exists(public_path().'/converted_files/')) {
                    mkdir(public_path().'/converted_files/', 0777);
                }
                $file_path = '/converted_files/'.$group_name.'_'.date("Y-m-d-H-i-s-a", time()).'.csv';
                $converted_writer->save(public_path().$file_path);
            }
            
            //Exception data
            $exception_file_path='';
            if(!empty($exception_excel_data) && $exception_excel_data >2){
                $expection_spreadsheet->getActiveSheet()->fromArray($exception_excel_data, NULL, 'A1');
                $expection_writer = new Xlsx($expection_spreadsheet);
                if (!file_exists(public_path().'/converted_files/')) {
                    mkdir(public_path().'/converted_files/', 0777);
                }
                $exception_file_path = '/converted_files/Exception_'.$group_name.'_'.date("Y-m-d-H-i-s-a", time()) .'.xlsx';
                $expection_writer->save(public_path().$exception_file_path);
            }
            
            $doo_exception_file_path='';
            if(!empty($doo_exception_excel_data) && $doo_exception_excel_data >2){
                $doo_expection_spreadsheet->getActiveSheet()->fromArray($doo_exception_excel_data, NULL, 'A1');
                $expection_writer = new Xlsx($doo_expection_spreadsheet);
                if (!file_exists(public_path().'/converted_files/')) {
                    mkdir(public_path().'/converted_files/', 0777);
                }
                $doo_exception_file_path = '/converted_files/DOO_'.$group_name.'_'.date("Y-m-d-H-i-s-a", time()) .'.xlsx';
                $expection_writer->save(public_path().$doo_exception_file_path);
            }
          
        return response()->json([
            'doo_file_path' => ($doo_exception_file_path ? "C://wamp64/www/hni_macro/public/".$doo_exception_file_path : ''),
            'file_path'=>($file_path ? "C://wamp64/www/hni_macro/public/".$file_path : ''),
            'exception_file_path' => ($exception_file_path ? "C://wamp64/www/hni_macro/public/".$exception_file_path : ''),
            'success' => true,
       ], 200);             
    }
}
    
    